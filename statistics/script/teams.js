jQuery(document).ready(function(){
	
	function getTeams(){
		$.getJSON(adr+"/teams", function(json) {
			if(json.code==0){
				//Succes
				var html ="";
				$.each(json.teams, function(key, val){
					html +="<tr class=\"clickable-row\" data-url=\"players.html?codeTeams="+val.NameTeam+"\"><td><img class=\"flag\" src=\""+val.URL+"\"></td><td>"+val.CompleteName+"</td><td>"+val.Points+"</td></tr>";
				});

				$("#teamsList").html(html);
			}else{
				//Echec
				console.log("Cannot retrieve the teams list, error : "+json.code);
			}
		});
	}

	getTeams();
	
	$(document).on('click','.clickable-row', function(){
        window.document.location = $(this).data("url");
	});
});
