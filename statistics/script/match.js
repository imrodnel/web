jQuery(document).ready(function(){
	 $.ajaxSetup({
	   async: false
	 });

	function getUrlParameter(){
	    var sPageURL = window.location.search.substring(1);
	    var sParameterName = sPageURL.split('=');
	    return sParameterName[1];
	}

	var id=getUrlParameter();

	function getMatchScore(){
		var html2="No Score";

		$.getJSON(adr+"/matches/"+id+"/score", function(json) {
			if(json.code==0){
				html2="<div class=\"leftScore\">";
				html2+="<div id=\"leftBlock\">";
				var nb1=1;
				$.each(json.sets, function(key,val){
					html2+="<div id=\"setA"+nb1+"\">"+val.A+"</div>";
					nb1++;
				});	

				// html2+="<div id=\"scoreA\">";
				// html2+=json.scoreA;
				// html2+="</div>";
				html2+="</div>";
				html2+="</div>";
				html2+="<div class=\"rightScore\">";
				html2+="<div id=\"rightBlock\">";
				var nb2=1;
				$.each(json.sets, function(key,val){
					html2+="<div id=\"setB"+nb2+"\">"+val.B+"</div>";
					nb2= nb2 + 1;
				});		

				// html2+="<div id=\"scoreB\">";
				// html2+=json.scoreB;	
				// html2+="</div>";
				html2+="</div>";
				html2+="</div>";
			}else{
				//Echec
				console.log("Cannot retrieve the match score, error : "+json.code);
			}
		});
		return html2;
	}

	function getMatchInfo(){
		$.getJSON(adr+"/matches/"+id, function(json) {
			if(json.code==0){
				//Succes
				var html ="";
				html+="<div class=\"firstContent\">";

				html+="<div class=\"leftPhoto\"><span class=\"helper\"></span><img class=\"photo\" src=\""+(json.match.UrlA_1==null?"http://5.196.21.161/photos/default.png":json.match.UrlA_1)+"\"/>"+((json.match.Category=="SM"||json.match.Category=="SW"?"":json.match.UrlA_2==null?"<img class=\"photo marginleft\" src=\"http://5.196.21.161/photos/default.png\"/>":" <img class=\"photo marginleft\" src=\""+json.match.UrlA_2+"\"/>"))+"</div>";
				html+="<div class=\"rightPhoto\"><span class=\"helper\"></span><img class=\"photo\" src=\""+(json.match.UrlB_1==null?"http://5.196.21.161/photos/default.png":json.match.UrlB_1)+"\"/>"+((json.match.Category=="SM"||json.match.Category=="SW"?"":json.match.UrlB_2==null?"<img class=\"photo marginleft\" src=\"http://5.196.21.161/photos/default.png\"/>":" <img class=\"photo marginleft\" src=\""+json.match.UrlB_2+"\"/>"))+"</div>";

				html+="<div class=\"blockScore\">";
				html+=getMatchScore();
				html+="</div>";

				html+="</div>";

				html+="<div class=\"leftName\">"+json.match.Player_A1+""+(json.match.Player_A2==null?"":"<br/>"+json.match.Player_A2)+"</div>";
				html+="<div class=\"rightName\">"+json.match.Player_B1+""+(json.match.Player_B2==null?"":"<br/>"+json.match.Player_B2)+"</div>";
				
				
				$("#playersView").html(html);
			}else{
				//Echec
				console.log("Cannot retrieve the match info, error : "+json.code);
			}
		});
	}

	function getMatchStats(){
		$.getJSON(adr+"/statistics/matches/"+id, function(json) {
			if(json.code==0){
				//Succes
				//Equipe A
				$("#AceA").html(json.statistics.A.NbAce);
				$("#DoubleFaultA").html(json.statistics.A.NbDoubleFault);
				$("#WinnerPointA").html(json.statistics.A.NbWinnerPoint);
				$("#UnforcedErrorA").html(json.statistics.A.NbUnforcedError);
				$("#ForcedErrorA").html(json.statistics.A.NbForcedError);
				$("#BreakWonA").html(json.statistics.A.NbWonBreak);
				$("#NbServeA").html(json.statistics.A.NbServe);
				$("#WonServeA").html(json.statistics.A.NbWonServe);
				$("#FirstServeFaultA").html(json.statistics.A.NbFirstServeFault);
				$("#SuccessfulFirstServeA").html(json.statistics.A.NbSuccessfulFirstServe);

				$("#aslr").html(json.statistics.A.NbLeftToRightWonAsServer+"/"+json.statistics.A.NbLeftToRightAsServer);
				$("#asrl").html(json.statistics.A.NbRightToLeftWonAsServer+"/"+json.statistics.A.NbRightToLeftAsServer);
				$("#arrl").html(json.statistics.A.NbRightToLeftWonAsReceiver+"/"+json.statistics.A.NbRightToLeftAsReceiver);
				$("#arlr").html(json.statistics.A.NbLeftToRightWonAsReceiver+"/"+json.statistics.A.NbLeftToRightAsReceiver);


				//Equipe B
				$("#AceB").html(json.statistics.B.NbAce);
				$("#DoubleFaultB").html(json.statistics.B.NbDoubleFault);
				$("#WinnerPointB").html(json.statistics.B.NbWinnerPoint);
				$("#UnforcedErrorB").html(json.statistics.B.NbUnforcedError);
				$("#ForcedErrorB").html(json.statistics.B.NbForcedError);
				$("#BreakWonB").html(json.statistics.B.NbWonBreak);
				$("#NbServeB").html(json.statistics.B.NbServe);
				$("#WonServeB").html(json.statistics.B.NbWonServe);
				$("#FirstServeFaultB").html(json.statistics.B.NbFirstServeFault);
				$("#SuccessfulFirstServeB").html(json.statistics.B.NbSuccessfulFirstServe);


				$("#brrl").html(json.statistics.B.NbRightToLeftWonAsReceiver+"/"+json.statistics.B.NbRightToLeftAsReceiver);
				$("#brlr").html(json.statistics.B.NbLeftToRightWonAsReceiver+"/"+json.statistics.B.NbLeftToRightAsReceiver);
				$("#bslr").html(json.statistics.B.NbLeftToRightWonAsServer+"/"+json.statistics.B.NbLeftToRightAsServer);
				$("#bsrl").html(json.statistics.B.NbRightToLeftWonAsServer+"/"+json.statistics.B.NbRightToLeftAsServer);

			}else{
				//Echec
				console.log("Cannot retrieve the match stats, error : "+json.code);
			}
		});
	}

	getMatchInfo();
	getMatchStats();

	$(document).on('click','.clickable-row', function(){
        window.document.location = $(this).data("url");
	});
});
