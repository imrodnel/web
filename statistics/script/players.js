jQuery(document).ready(function(){

	function getUrlParameter(){
	    var sPageURL = window.location.search.substring(1);
	    var sParameterName = sPageURL.split('=');
	    return sParameterName[1];
	}

	var id=getUrlParameter();

	function getPlayers(){
		$.getJSON(adr+"/teams/"+id+"/players?X", function(json) {
			if(json.code==0){
				//Succes
				var html ="";
				$.each(json.players, function(key, val){
					html +="<tr class=\"clickable-row\" data-url=\"player.html?idJoueur="+val.IdPlayer+"\"><td><img class=\"photo\" src=\""+(val.Url==null?"http://5.196.21.161/photos/default.png":val.Url)+"\"></td><td>"+val.Name+"</td><td>"+val.FirstName+"</td><td>"+val.Sex+"</td><td>"+val.DateBirth+"</td></tr>";
				});

				$("#playersList").html(html);
			}else{
				//Echec
				console.log("Cannot retrieve the players list, error :  "+json.code);
			}
		});
	}

	getPlayers();
	
	$(document).on('click','.clickable-row', function(){
        window.document.location = $(this).data("url");
	});
});
