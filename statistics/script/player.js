jQuery(document).ready(function(){
	$.ajaxSetup({
	   async: false
	 });

	function getUrlParameter(){
	    var sPageURL = window.location.search.substring(1);
	    var sParameterName = sPageURL.split('=');
	    return sParameterName[1];
	}

	var id=getUrlParameter();
	function playerInfo(){
		$.getJSON(adr+"/players/"+id, function(json) {
			if(json.code==0){
				//Succes
				$("#photo").html("<img class=\"photo\" src=\""+(json.player.Url==null?"http://5.196.21.161/photos/default.png":json.player.Url)+"\">");
				$("#info").html(json.player.Name+" "+json.player.FirstName+"<br/> "+json.player.DateBirth);
				playerId=json.player.Id;
				//Team info
				$.getJSON(adr+"/teams/"+json.player.Team, function(json) {
					if(json.code==0){
						//Succes
						$("#flag").html("<img class=\"flag\" src=\""+json.team.URL+"\">");
					}else{
						//Echec
						console.log("Cannot retrieve the team info, error : "+json.code);
					}
				});
				
			}else{
				//Echec
				console.log("Cannot retrieve the player info, error : "+json.code);
			}
		});
	}

	function getStats(){
		$.getJSON(adr+"/statistics/players/"+id, function(json) {
			if(json.code==0){
				//Succes

				//Global stats
				$("#Ace").html(json.statistics.NbAce);
				$("#WinnerPoint").html(json.statistics.NbWinnerPoint);
				$("#DoubleFault").html(json.statistics.NbDoubleFault);
				$("#UnforcedError").html(json.statistics.NbUnforcedError);
				$("#ForcedError").html(json.statistics.NbForcedError);
				$("#BreakWon").html(json.statistics.NbWonBreak);
				$("#NbServe").html(json.statistics.NbServe);
				$("#WonServe").html(json.statistics.NbWonServe);
				$("#FirstServeFault").html(json.statistics.NbFirstServeFault);
				$("#SuccessfulFirstServe").html(json.statistics.NbSuccessfulFirstServe);

				//Average stats
				var nbMatch = json.statistics.NbMatches;
				$("#average").html(nbMatch+" matches");
				$("#Ace").next().html((json.statistics.NbAce/nbMatch).toFixed(2));
				$("#WinnerPoint").next().html((json.statistics.NbWinnerPoint/nbMatch).toFixed(2));
				$("#DoubleFault").next().html((json.statistics.NbDoubleFault/nbMatch).toFixed(2));
				$("#UnforcedError").next().html((json.statistics.NbUnforcedError/nbMatch).toFixed(2));
				$("#ForcedError").next().html((json.statistics.NbForcedError/nbMatch).toFixed(2));
				$("#BreakWon").next().html((json.statistics.NbWonBreak/nbMatch).toFixed(2));
				$("#NbServe").next().html((json.statistics.NbServe/nbMatch).toFixed(2));
				$("#WonServe").next().html((json.statistics.NbWonServe/nbMatch).toFixed(2));
				$("#FirstServeFault").next().html((json.statistics.NbFirstServeFault/nbMatch).toFixed(2));
				$("#SuccessfulFirstServe").next().html((json.statistics.NbSuccessfulFirstServe/nbMatch).toFixed(2));

			}else{
				//Echec
				console.log("Cannot retrieve the player statistics, error : "+json.code);
			}
		});
	}

	function getMatchs(){
		var html="";
		$.getJSON(adr+"/players/"+id+"/matches", function(json) {
			if(json.code==0){
				//Succes
				$.each(json.matches, function(key, val){
					// Récupération du score
					var score="";
					$.each(val.sets, function(key, val2){
						score+="&nbsp"+val2.A+"&nbsp";
					});
					score+="<br/>";
					$.each(val.sets, function(key, val2){
						score+="&nbsp"+val2.B+"&nbsp";
					});

					html+="<div class=\"card\" data-url=\"match.html?idMatch="+val.matchId+"\">";

					
					// Table
					html+="<table>";
					if (val.playerIdA1 == id || val.playerIdA2 == id) {
						html+="<tr><th><img class=\"flag3\" src=\""+val.teamFlagA+"\"></th><th><p>Versus</p></th><th><img class=\"flag3\" src=\""+val.teamFlagB+"\"></th></tr>";
						if (val.category=="SM"||val.category=="SW") {
							html+="<tr><th><img class=\"photo2\" src=\""+(val.playerPhotoA1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoA1)+"\"/></th><th>"+score+"</th><th><img class=\"photo2\" src=\""+(val.playerPhotoB1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoB1)+"\"/></th></tr>";
							html+="<tr><th>"+val.playerNameA1+"</th><th>"+(val.winner=="A"?"Won":"Lost")+"</th><th>"+val.playerNameB1+"</th></tr>";
						}else{
							html+="<tr><th><img class=\"photo2\" src=\""+(val.playerPhotoA1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoA1)+"\"/><img class=\"photo2 marginleft\" src=\""+(val.playerPhotoA2==null?"http://5.196.21.161/photos/default.png":val.playerPhotoA2)+"\"/></th><th>"+score+"</th><th><img class=\"photo2\" src=\""+(val.playerPhotoB1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoB1)+"\"/><img class=\"photo2 marginleft\" src=\""+(val.playerPhotoB2==null?"http://5.196.21.161/photos/default.png":val.playerPhotoB2)+"\"/></th></tr>";
							html+="<tr><th>"+val.playerNameA1+"</th><th></th><th>"+val.playerNameB1+"</th></tr>";
							html+="<tr><th>"+val.playerNameA2+"</th><th>"+(val.winner=="A"?"Won":"Lost")+"</th><th>"+val.playerNameB2+"</th></tr>";
						}
					}else{
						html+="<tr><th><img class=\"flag3\" src=\""+val.teamFlagB+"\"></th><th><p>Versus</p></th><th><img class=\"flag3\" src=\""+val.teamFlagA+"\"></th></tr>";
						if( val.category=="SM"||val.category=="SW" ){
							html+="<tr><th><img class=\"photo2\" src=\""+(val.playerPhotoB1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoB1)+"\"/></th><th>"+score+"</th><th><img class=\"photo2\" src=\""+(val.playerPhotoA1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoA1)+"\"/></th></tr>";
							html+="<tr><th>"+val.playerNameB1+"</th><th>"+(val.winner=="B"?"Won":"Lost")+"</th><th>"+val.playerNameA1+"</th></tr>";
						}else{
							html+="<tr><th><img class=\"photo2\" src=\""+(val.playerPhotoB1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoB1)+"\"/><img class=\"photo2 marginleft\" src=\""+(val.playerPhotoB2==null?"http://5.196.21.161/photos/default.png":val.playerPhotoB2)+"\"/></th><th>"+score+"</th><th><img class=\"photo2\" src=\""+(val.playerPhotoA1==null?"http://5.196.21.161/photos/default.png":val.playerPhotoA1)+"\"/><img class=\"photo2 marginleft\" src=\""+(val.playerPhotoA2==null?"http://5.196.21.161/photos/default.png":val.playerPhotoA2)+"\"/></th></tr>";
							html+="<tr><th>"+val.playerNameB1+"</th><th></th><th>"+val.playerNameA1+"</th></tr>";
							html+="<tr><th>"+val.playerNameB2+"</th><th>"+(val.winner=="B"?"Won":"Lost")+"</th><th>"+val.playerNameA2+"</th></tr>";
						}
					}

					html+="</table>";
					html+="</div>";
				});
				$("#match").html(html);
			}else{
				//Echec
				console.log("Cannot retrieve the player matches, error : "+json.code);
			}
		});
	}
	playerInfo();
	getStats();
	getMatchs();

	$("#statistique").click(function () {

	    $header = $(this);
	    //getting the next element
	    $content = $header.next();
	    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
	    $content.slideToggle(500, function () {
	        //execute this after slideToggle is done
	    });
	});

	$("#matches").click(function () {

	    $header = $(this);
	    //getting the next element
	    $content = $header.next();
	    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
	    $content.slideToggle(500, function () {
	        //execute this after slideToggle is done
	    });
	});

	$(document).on('click','.card', function(){
        window.document.location = $(this).data("url");
	});
});