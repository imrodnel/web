jQuery(document).ready(function()
{
	
	var adr = adresse + "/supermatches?round=CURRENT" ;
	var tab = new Array() ; // permet de connaitre ou se situe la rencontre parmis les colonnes
	
	var tabRencontre = new Array() ; // contient le nombre de set des différent matchs
	
	
	var numRen;
	var nbRen;
	
	// on initialise tabRencontre
	function initTab() 
	{		
		for (i = 0; i < 4; i++) 
		{ 
			tabRencontre[i] = new Array();
		}
		for (i = 0; i < 4; i++) 
		{ 
			for (j = 0; j < 7; j++) 
			{ 
				tabRencontre[i][j] = 0;
			}
		}
	}
	
	
	// On recupere les infos concernant les different matches et on ajout dans match 
	
	function construction() 
	{ 
		var numSet = 1; // contiendra le nombre de set;
		var numMatch = 0; 
		
		// on récupere le fichier json
		$.getJSON(adr, function(json)
		{
			var idRen = 0 ; //indice de la rencontre
			//$("#erreur2").append("<br/>ici 1 ");
			// on parcours les differentes rencontres
			$.each(json.supermatches, function(key, renc)
			{
				//$("#erreur2").append("<br/>rencontre : " + renc.id);
				tab[idRen] = renc.id;
				numMatch = 0; // permet de savoir quel match on traite
				
				$("#R"+(idRen+1)+"titrePays").html("<img src='"+renc.teamFlagA+"' class='drapeauSupermatch'>  <span class='teamNames'> " + renc.teamNameA +" "+renc.teamScoreA+" - "+renc.teamScoreB+" "+ renc.teamNameB + "</span> <img src='"+renc.teamFlagB+"' class='drapeauSupermatch'></br><span class='soustitre'>" + afftable(renc.tableau) + "</span>");
				
				$.each(renc.matches, function(key, val)
				{
					numMatch = Match_emplacement(val.category, val.id);
					
					// affiche le titre du match
					if(val.category == "SM")
						nom = nomMatch(val.category) + " N° " + (val.id%7) ;
					else if(val.category == "SW")
						nom = nomMatch(val.category) + " N° " + ((val.id-2)%7) ;
					else 
						nom = nomMatch(val.category);
					
					if(val.status == "PROGRESS")
					{
						$("#R"+(idRen+1)+"match"+numMatch).html(nom + " - " + courtNum(val.court));
					}
					else
					{
						$("#R"+(idRen+1)+"match"+numMatch).html(nom);
					}
					
					if(val.status == "PROGRESS" || val.status == "FINISH")
					{
						$("#tableR"+(idRen+1)+"match"+numMatch).show("fast");
						$("#tableR"+(idRen+1)+"soon"+numMatch).hide("fast");
						
						if(val.category == "SM" || val.category == "SW")
						{
							$("#R"+(idRen+1)+"joueurAmatch"+numMatch).html(nomJoueur(val.playerNameA1));
							$("#R"+(idRen+1)+"joueurBmatch"+numMatch).html(nomJoueur(val.playerNameB1));
						}
						else
						{
							$("#R"+(idRen+1)+"joueurAmatch"+numMatch).html(doubleJoueur(val.playerNameA1) + "<br/>" + doubleJoueur(val.playerNameA2));
							$("#R"+(idRen+1)+"joueurBmatch"+numMatch).html(doubleJoueur(val.playerNameB1) + "<br/>" + doubleJoueur(val.playerNameB2));
						}
						
						if(val.winner == "A")
						{
							$("#R"+(idRen+1)+"balleAmatch"+numMatch).html("<img src='images/w.png' class='balle' />");
							$("#R"+(idRen+1)+"balleBmatch"+numMatch).html("");
						}
						else if(val.winner == "B")
						{
							$("#R"+(idRen+1)+"balleAmatch"+numMatch).html("");
							$("#R"+(idRen+1)+"balleBmatch"+numMatch).html("<img src='images/w.png' class='balle' />");
						}
						else 
						{
							if(val.service == "A")
							{
								$("#R"+(idRen+1)+"balleAmatch"+numMatch).html("<img src='images/BT.png' class='balle' />");
								$("#R"+(idRen+1)+"balleBmatch"+numMatch).html("");
							}
							else if(val.service == "B")
							{
								$("#R"+(idRen+1)+"balleAmatch"+numMatch).html("");
								$("#R"+(idRen+1)+"balleBmatch"+numMatch).html("<img src='images/BT.png' class='balle' />");
							}
						
						}
						
						if(val.winner == "A"){
							$("#R"+(idRen+1)+"joueurAmatch"+numMatch).toggleClass("nomJGagne", true);
						}
						else{
							$("#R"+(idRen+1)+"joueurAmatch"+numMatch).toggleClass("nameJoueurA", true);
						}
						
						if(val.winner == "B"){
							$("#R"+(idRen+1)+"joueurBmatch"+numMatch).toggleClass("nomJGagne", true);
						}
						else{
							$("#R"+(idRen+1)+"joueurBmatch"+numMatch).toggleClass("nameJoueurB", true);
						}

						if(val.winner == "A" || val.winner == "B"){
							$("#R"+(idRen+1)+"scoreAmatch"+numMatch).hide("fast");
							$("#R"+(idRen+1)+"scoreBmatch"+numMatch).hide("fast");
						} else {
							$("#R"+(idRen+1)+"scoreAmatch"+numMatch).show("fast");
							$("#R"+(idRen+1)+"scoreBmatch"+numMatch).show("fast");	
						}
						
						$("#R"+(idRen+1)+"scoreAmatch"+numMatch).html(score(val.scoreA));
						$("#R"+(idRen+1)+"scoreBmatch"+numMatch).html(score(val.scoreB));
						
						numSet = 1;

						$.each(val.sets, function(set, score_set)
						{
							if((numSet == 1) && (tabRencontre[idRen][(numMatch-1)] < numSet )) // on test si le set a deja commencer
							{
								$("#R"+(idRen+1)+"balleAmatch"+numMatch).after("<td class='tdscoreset' id='R"+(idRen+1)+"match"+numMatch+"setA"+numSet+"'>"+score_set.A+"</td>");
								$("#R"+(idRen+1)+"balleBmatch"+numMatch).after("<td class='tdscoreset' id='R"+(idRen+1)+"match"+numMatch+"setB"+numSet+"'>"+score_set.B+"</td>");
								tabRencontre[idRen][numMatch-1]++;
							}
							else
							{
								if(tabRencontre[idRen][(numMatch-1)] < numSet)
								{
									$("#R"+(idRen+1)+"match"+numMatch+"setA"+(numSet - 1)).after("<td class='tdscoreset' id='R"+(idRen+1)+"match"+numMatch+"setA"+numSet+"'>"+score_set.A+"</td>");
									$("#R"+(idRen+1)+"match"+numMatch+"setB"+(numSet - 1)).after("<td class='tdscoreset' id='R"+(idRen+1)+"match"+numMatch+"setB"+numSet+"'>"+score_set.B+"</td>");
									tabRencontre[idRen][numMatch-1] = tabRencontre[idRen][numMatch-1] + 1;
								}
								else
								{
									$("#R"+(idRen+1)+"match"+numMatch+"setA"+numSet).html(score_set.A);
									$("#R"+(idRen+1)+"match"+numMatch+"setB"+numSet).html(score_set.B);
								}
							}
							numSet++;
						});
					}
					else
					{
						$("#tableR"+(idRen+1)+"match"+numMatch).hide("fast");
						$("#tableR"+(idRen+1)+"soon"+numMatch).show("fast");
						$("#R"+(idRen+1)+"balleAmatch"+numMatch).html("");
						$("#R"+(idRen+1)+"balleBmatch"+numMatch).html("");
					}
				});

				idRen++;

			});

		});
	}
	
	
	function init() 
	{
		initTab();
		construction();
		setInterval(construction,3000);
	}
	
	init();
	
	
	
	function score(nb) 
	{
		if(nb == 4444)
		{
			nb = 'ADV';
		}
		return nb ;
	}

	function NowRencontre(Ren)
	{
		var result = 0 ;
		var trouver = 0 ;
		var i = 0;
		// $("#erreur").append("<br/>NowRencontre Ren : " + Ren);
		while( i<4 && trouver != 1 )
		{	
			// $("#erreur").append("<br/> tab[" + i + "] : " + tab[i]);
			if(tab[i] == Ren)
			{
					result = i;
					trouver = 1 ;
			}
			else
				i++;
		}
		return i ;
	}

	function Match_emplacement(typeMatch, IdMatch) 
	{ 
		var numM = 0 ;
		switch (typeMatch) 
		{
			case "SM":
				if((IdMatch%7) == 1)
				{
					numM = 1;
				}
				else
				{
					numM = 2;
				}
			break;
			case "SW":
				if(((IdMatch-2)%7) == 1)
				{
					numM = 3;
				}
				else
				{
					numM = 4;
				}
			break;
			case "DX":
				numM = 7;
			break;
			case "DW":
				numM = 6;
			break;
			case "DM":
				numM = 5;
			break;
			
		}
		
		return numM ;
	
	}
	
	
	
	function nomMatch(Categorie) 
	{
		switch (Categorie) 
		{
			case "SM":
				Categorie = "Men's Single";
			break;
			case "SW":
				Categorie = "Women's Single";
			break;
			case "DX":
				Categorie = "Mixed Double";
			break;
			case "DW":
				Categorie = "Women's Double";
			break;
			case "DM":
				Categorie = "Men's Double";
			break;
			
		}
		
		return Categorie ;
	}
	
	
	function afftable(t) 
	{
		switch (t) 
		{
			case "1/4-P":
				t = "Quarterfinals";
			break;
			case "1/2-P":
				t = "Semifinals";
			break;
			case "1/2-5":
				t = "5-8th place";
			break;
			case "Finale-P":
				t = "Finals";
			break;
			case "Finale-3":
				t = "3rd - 4th place";
			break;
			case "Finale-5":
				t = "5-6th place";
			break;
			case "Finale-7":
				t = "7-8th place";
			break;
			
		}
		
		return t ;
	}
	
	
	 //la fonction prend en paramettre nation. on l'appelle avec 'json.nat' 
	function pays(nation)
	{   
		var c = "";  
		switch(nation) 
		{
			case "FRA":
				c = "France";
				break;
			case "USA":
				c = "USA";
				break;
			case "GBR":
				c = "Great Britain";
				break;
			case "CHN":
				c = "China";
				break;
			case "DEU":
				c = "Germany";
				break;
			case "BEL":
				c = "Belgium";
				break;
			case "IRL":
				c = "Ireland";
				break;
			case "RUS":
				c = "Russia";
				break;
			default: c = "";
		}
		
		return c ;
	}
	 function courtNum(numC)
	{   
		var c = "";  
		switch(numC) 
		{
			case 1:
				c = "Main court";
				break;
			case 2:
				c = "Court 1";
				break;
			case 3:
				c = "Court 2";
				break;
			case 4:
				c = "Court 3";
				break;
			case 5:
				c = "Court 4";
				break;
			case 6:
				c = "Court 5";
				break;
			case 7:
				c = "Court 6";
				break;
			default: c = "";
		}
		
		return c ;
	}
	
	function nomJoueur(nomJ)
	{   
		return nomJ;
		var nomJFinal = "";  
		if(nomJ.length > 12)
			nomJFinal = nomJ.substring(0,11) + ".";
		else
			nomJFinal = nomJ;
		
		return nomJFinal ;
	}
	
	function doubleJoueur(nomJ)
	{   
		return nomJ;
		var nomJFinal = "";  
		if(nomJ.length > 6)
			nomJFinal = nomJ.substring(0,5) + ".";
		else
			nomJFinal = nomJ;
		
		return nomJFinal ;
	}
	
});