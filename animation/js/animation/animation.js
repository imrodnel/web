var PageTransitions = (function() {
	
	var $main = $( '#pt-main' ),
		$pages = $main.children( 'div.pt-page' ),
		$iterate = $( '#iterateEffects' ),
		pagesCount = $pages.length,
		current = 0,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		support = Modernizr.cssanimations;;
	function init() 
	{
		$pages.each( function() 
		{
			var $page = $( this );
			$page.data( 'originalClassList', $page.attr( 'class' ) );
		} );

		$pages.eq( current ).addClass( 'pt-page-current' );
		$iterate.on( 'click', function(){nextPage();} );
	}
	
	
	function nextPage() 
	{
		var $currPage = $pages.eq( current );

		if( current < pagesCount - 1 ) 
		{
			++current;
		}
		else 
		{
			current = 0;
		}

		var $nextPage = $pages.eq( current ).addClass( 'pt-page-current' );

		outClass = 'pt-page-rotateRoomTopOut pt-page-ontop';
		inClass = 'pt-page-rotateRoomTopIn';

		$currPage.addClass(outClass).on( animEndEventName, function() 
		{
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) 
			{
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}

	}
	
	function onEndAnimation( $outpage, $inpage ) 
	{
		endCurrPage = false;
		endNextPage = false;
		resetPage( $outpage, $inpage );
	}
	
	function resetPage( $outpage, $inpage ) {
		$outpage.attr( 'class', $outpage.data( 'originalClassList' ) );
		$inpage.attr( 'class', $inpage.data( 'originalClassList' ) + ' pt-page-current' );
	}
	
	init();
	//setInterval(nextPage,20000);
	
	
})();