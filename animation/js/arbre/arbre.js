jQuery(document).ready(function()
{
	var adr=  adresse + "/bracket";
	//var adr="./arbre.json";
	
	function tableau()
	{
		$.getJSON(adr, function(json)
		{
		    if(json.code==0){
				$.each(json.bracket, function( key, val )
				{  
					if((val.Score_A+val.Score_B)==7){
						if(val.Score_A>val.Score_B ){
						$("#m"+val.IdHugeMatch).html("<div class='pays'><div class='gauche'><img class='drapeaux' src='"+val.UrlTeam_A+"' align='left'><span class='textegagnant' id='paysA_finalLoser'>"+val.NameTeam_A+"</span>	</div><div class='droite'><img class='drapeaux' src='"+val.UrlTeam_B+"' align='left'><span class='texteperdant' id='paysB_finalLoser'>"+val.NameTeam_B+"</span> </div></div><p align='center' ><span class='scoregagnant' >"+val.Score_A+"</span>  - <span class='scoreperdant'> "+val.Score_B+"</span> </p>");
						}
						else{
						$("#m"+val.IdHugeMatch).html("<div class='pays'><div class='gauche'><img class='drapeaux' src='"+val.UrlTeam_A+"' align='left'><span class='texteperdant' id='paysA_finalLoser'>"+val.NameTeam_A+"</span>	</div><div class='droite'><img class='drapeaux' src='"+val.UrlTeam_B+"' align='left'><span class='textegagnant' id='paysB_finalLoser'>"+val.NameTeam_B+"</span> </div></div><p align='center' ><span class='scoreperdant' >"+val.Score_A+"</span>  - <span class='scoregagnant'> "+val.Score_B+"</span> </p>");
						}
					}
				  else{
						$("#m"+val.IdHugeMatch).html("<div class='pays'><div class='gauche'><img class='drapeaux' src='"+val.UrlTeam_A+"' align='left'><span class='texte' id='paysA_finalLoser'>"+val.NameTeam_A+"</span>	</div><div class='droite'><img class='drapeaux' src='"+val.UrlTeam_B+"' align='left'><span class='texte' id='paysB_finalLoser'>"+val.NameTeam_B+"</span> </div></div><p align='center' ><span class='score' >"+val.Score_A+"</span>  - <span class='score'> "+val.Score_B+"</span> </p>");
					} 
				});
			}
			else{
				$.each(json.bracket, function( key, val )
				{ 
					$("#m"+val.IdHugeMatch).html("");
				});
			}
		});
	}
	
	tableau();
	setInterval(tableau, 2000);
});