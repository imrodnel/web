

jQuery(document).ready(function()
{      		var tab = [1, 2, 3, 4, 5, 6, 7];
			var nbsetB = [1,1,1,1,1,1,1,1];
			url = adresse+"/courts/infos/" ;
			//var adr ="fichier.json";
			var adr;
			var i=0;
		    var nomJoueurB="";
			var nomJoueurA="";
			var nomJoueurB="";
			var nomJFinalA="";
			var nomJFinalB="";
		   
		 //remplissage des tableaux et mise a jours toutes les 2 secondes
		 function terrain() 
		 {
			$.each(tab, function (indice, j)
			{
				adr=url+j;
				$.getJSON(adr, function(json)
					{ 	
						var code= json.code;
						if(code!=0){
						   	$("#time"+j).html("");
						   	$("#cat"+j).html("");
						   	$("#natA"+j).html("");
						   	$("#natB"+j).html("");
						   	$("#natA"+j).html("");
						   	$("#serviceA"+j).html("");
						   	$("#serviceB"+j).html("");
						   	$("#nomA1"+j).html("");
						   	$("#nomA2"+j).html("");
						   	$("#nomB1"+j).html("");
						   	$("#nomB2"+j).html("");
						   	$("#scoreA"+j).html("");
						   	$("#scoreB"+j).html("");
						   	$("#setA"+j+1).html("");
						   	$("#setB"+j+1).html("");
						   	$("#setA"+j+2).html("");
						   	$("#setB"+j+2).html("");
						   	$("#setA"+j+3).html("");
						   	$("#setB"+j+3).html("");
						   	$("#setA"+j+4).html("");
						   	$("#setB"+j+4).html("");
						   	$("#setA"+j+5).html("");
						   	$("#setB"+j+5).html("");
						   	$("#photoA"+j).html("<img src='images/person.png' width='86' height='114'>");
						   	$("#photoB"+j).html("<img src='images/person.png' width='86' height='114'>");
							$("#scoreA"+j).hide("fast");
							$("#scoreB"+j).hide("fast");	
						}
						else{
							var court= json.court;
							//chargement des photos des joueurs
							if(court.category=="DM"||court.category=="DW"||court.category=="DM"){
								var time = court.duration;
								var step = "3";
								var minutes = time.split(":")[1];
								var k = Math.floor(minutes / step);
								
								if(k % 2 == 0){
									var photoA= court.playerPhotoA1;
									if(photoA == null)
										photoA = "images/person.png";
									$("#photoA"+j).html("<img src='"+photoA+"'alt='"+court.TeamA+"' width='86' height='114'>");
									var photoB= court.playerPhotoB1;
									if(photoB == null)
										photoB = "images/person.png";
									$("#photoB"+j).html("<img src='"+photoB+"'alt='"+court.TeamB+"' width='86' height='114'>");
								}
								else{
									var photoA= court.playerPhotoA2;
									if(photoA == null)
										photoA = "images/person.png";
									$("#photoA"+j).html("<img src='"+photoA+"' alt='"+court.TeamA+"' width='86' height='114'>");
									var photoB= court.playerPhotoB2;
									if(photoB == null)
										photoB = "images/person.png";
									$("#photoB"+j).html("<img src='"+photoB+"' alt='"+court.TeamB+"' width='86' height='114'>");
								}
							}
							else{
								var photoA= court.playerPhotoA1;
								if(photoA == null)
									photoA = "images/person.png";
								$("#photoA"+j).html("<img src='"+photoA+"' alt='"+court.TeamA+"'width='86' height='114'>");
								
								var photoB= court.playerPhotoB1;
								if(photoB == null)
									photoB = "images/person.png";
								$("#photoB"+j).html("<img src='"+photoB+"' alt='"+court.TeamB+"'width='86' height='114'>");
								
							}//fin chargement des photos
							
							if(court.category=="DM"||court.category=="DW"||court.category=="DX"){	
								$("#nomA1"+j).html(court.playerNameA1);
								$("#nomA2"+j).html(court.playerNameA2);
								$("#nomB1"+j).html(court.playerNameB1);
								$("#nomB2"+j).html(court.playerNameB2);
							}
							else{
								$("#nomA1"+j).html(court.playerNameA1);
						   		$("#nomA2"+j).html("");
								$("#nomB1"+j).html(court.playerNameB1);
						   		$("#nomB2"+j).html("");
							}
							//fin chargement des noms
							
							//chargement du type de match
							switch(court.category) {
										case "SM":
											$("#cat"+j).html("<i>Men's Single</i> | ");
											break; 
										case "SW":
											$("#cat"+j).html("<i>Women's Single</i> | ");
											break;
										case "DM":
											$("#cat"+j).html("<i>Men's Double</i> | ");
											break;
										case "DW":
											$("#cat"+j).html("<i>Women's Double</i> | ");
											break;
										case "DX":
											$("#cat"+j).html("<i>Mixed Double</i> | ");//
											break;
										default: $("#cat"+j).html("");
							}// fin 
							
							//duree du match
							$("#time"+j).html("<i>"+court.duration+"</i>");
								
							
							//nationnalité des joueurs
							if (court.teamFlagA!=null){
								$("#natA"+j).html("<img src='"+court.teamFlagA+"' alt='"+court.teamFlagA+"' width='38px'/>");
								$("#natB"+j).html("<img src='"+court.teamFlagB+"' alt='"+court.teamFlagB+"' width='38px'/>");
							}
							else{
								$("#natA"+j).html("<img src='+http://5.196.21.161/drapeaux/default.png+' alt='"+court.teamFlagA+"'  width='38px'/>");
								$("#natB"+j).html("<img src='+http://5.196.21.161/drapeaux/default.png+' alt='"+court.teamFlagB+"'  width='38px'/>");
							}
							
								
							//chargement des scores
							if(court.winner==null){
								$("#scoreA"+j).show("fast");
								$("#scoreB"+j).show("fast");
								if(court.scoreA== 4444){
									$("#scoreA"+j).html("AD");
								}
								else
									{
									 $("#scoreA"+j).html(court.scoreA);
								}
								if(court.scoreB== 4444){
									$("#scoreB"+j).html("AD");
								}
								else{
									 $("#scoreB"+j).html(court.scoreB); 
								}

								// qui a le service 
								if(court.service=="A"){
									$("#serviceA"+j).html("<img src='images/BT.png' alt='sevice'  width='25px'/>");
									$("#serviceB"+j).html("");
								}else {
									$("#serviceB"+j).html("<img src='images/BT.png' alt='sevice'  width='25px'/>");
									$("#serviceA"+j).html("");
								}//fin
							}
							else{
								$("#scoreA"+j).hide("fast");
								$("#scoreB"+j).hide("fast");
								if(court.winner == "A"){
									$("#serviceA"+j).html("<img src='images/w.png' alt='sevice'  width='25px'/>");
									$("#serviceB"+j).html("");
								}else {
									$("#serviceA"+j).html("");
									$("#serviceB"+j).html("<img src='images/w.png' alt='sevice'  width='25px'/>");
								}
							}//fin chargement des scores
							
							// chargement des sets 
							$.each( court.set, function( key, val )
							{
								i++;
								$("#setA"+j+i).html(val.A);
								$("#setB"+j+i).html(val.B);
								
							});
						
							while(i>nbsetB[j]){
								$("#setA"+j+nbsetB[j]).after("<td id='setA"+j+(nbsetB[j]+1)+"' class='set'>"+court.set[nbsetB[j]].A+"</td>");
								$("#setB"+j+nbsetB[j]).after("<td id='setB"+j+(nbsetB[j]+1)+"' class='set'>"+court.set[nbsetB[j]].B+"</td>");
								nbsetB[j]++;
							}
							while(i<nbsetB[j]){
								$("#setA"+j+nbsetB[j]).remove();
								$("#setB"+j+nbsetB[j]).remove();
								nbsetB[j]--;
							}	
							i=0;
							// fin chargement des sets 
						}
					});
				});
		}
		//appelle de la fonction terrain toutes les secondes
		
		// Met a jour le "titre" de la page (indique le round courrant du tournoi)
		function updateCurrentRound(){
			var tournamentUrl = adresse + "/tournament";
				$.getJSON(tournamentUrl, function(json) {
					switch(json.tournament.Stage) {
						case "1/4": $("#tableau").html("QUARTERFINAL"); break;
						case "1/2": $("#tableau").html("SEMIFINAL"); break;
						case "Final": $("#tableau").html("FINAL"); break;
						case "SETTING-UP": $("#tableau").html("SOON"); break;
						default: $("#tableau").html("");
					}
				});
		}

		terrain();
		updateCurrentRound();
		setInterval(terrain, 2000);
		setInterval(updateCurrentRound, 2000);
});