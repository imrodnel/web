   //2*120    
jQuery(document).ready(function()
{           var tab = [1, 2, 3, 4, 5, 6, 7];
			var nbsetA = [1,1,1,1,1,1,1];
			var nbsetB = [1,1,1,1,1,1,1];
			var n = [1,1,1,1,1,1,1];
			var url = "http://5.196.21.161:8080/middlewareTennis/court/info/" ;
			var adr ;
			var i=0;
		
	
		   
		// remplissage des tableaux et mise a jours toutes les 2 secondes
		function terrain() 
		{
			$.each(tab, function (indice, j)
			{
			    adr=url+j;
				$.getJSON(adr, function(json)
				{
				      if(json.code=="-1")
					  {
					   $("#cat"+j).html("no match in progress");
					   $("#cat"+j).html("");
					   $("#natA"+j).html("");
					   $("#natB"+j).html("");
					   $("#natA"+j).html("");
					   $("#serviceA"+j).html("");
					   $("#serviceB"+j).html("");
					   $("#nomA"+j).html("");
					   $("#nomB"+j).html("");
					   $("#scoreA"+j).html("");
					   $("#scoreB"+j).html("");
					   $("#setA"+j+1).html("");
					   $("#setB"+j+1).html("");
					  }
					  else
					  {
							switch(json.natureA) {
                                case "SH":
                                    $("#cat"+j).html("<i>Men's Single</i><br/>"+json.duree+"");
                                    break;
                                case "SF":
                                    $("#cat"+j).html("<i>Women's Single</i><br/>"+json.duree+"");
                                    break;
                                case "DH":
                                    $("#cat"+j).html("<i>Men's Double</i><br/>"+json.duree+"");
                                    break;
                                case "DF":
                                    $("#cat"+j).html("<i>Women's Double</i><br/>"+json.duree+"");
                                    break;
                                case "DM":
                                    $("#cat"+j).html("<i>Mixted Double</i><br/>"+json.duree+"");
                                    break;
                                default: $("#cat"+j).html("");
                            }
						$("#natA"+j).html("<img src='image/"+json.nationaliteA+".jpg' alt='"+json.nationaliteA+"'height='30px' width='30px'/>");
						$("#natB"+j).html("<img src='image/"+json.nationaliteB+".jpg' alt='"+json.nationaliteB+"'height='30px' width='30px'/>");
						
							var nomJA = json.nomJoueurA;
							var nomJB = json.nomJoueurB;
							var nomJFinalA = ""; // sera le nom final qu on affichera a l ecran
							var nomJFinalB = "";
							
							if(nomJB.length > 20)
							{
								 nomJB = nomJB.split("/");
								 for(var e = 0 ; e < nomJB.length; e++)
								 {
									if(nomJB[e].length > 10)
									{
										nomJFinalB = nomJFinalB + nomJB[e].substring(0,7) + ".";
									}
									else
										nomJFinalB = nomJFinalB + nomJB[e];
									if(e==0)
										nomJFinalB = nomJFinalB + "/";
								 }
								 
								
							}
							else
							{
								nomJFinalB = nomJB;
							}
							if(nomJA.length >= 20)
							{
									nomJA = nomJA.split("/");
								 for(var e = 0 ; e < nomJA.length; e++)
								 {
									if(nomJA[e].length > 10)
										nomJFinalA = nomJFinalA + nomJA[e].substring(0,7) + ".";
									else
										nomJFinalA = nomJFinalA + nomJA[e];
									if(e==0)
										nomJFinalA = nomJFinalA + "/";
								 }
							}
							else
							{
								nomJFinalA = nomJA;
							}
						
						
						if(json.vainqueur=="A")
						{
							$("#nomA"+j).replaceWith("<td  id= 'nomA"+j+"' class='name gagnant'>"+nomJFinalA+"</td>");
						}
						else
						{
							$("#nomA"+j).html(nomJFinalA);
						}
						if(json.vainqueur=="B")
						{
							$("#nomB"+j).replaceWith("<td  id= 'nomA"+j+"' class='name gagnant'>"+nomJFinalB+"</td>");
						}
						else
						{
							$("#nomB"+j).html(nomJFinalB);
						}
						//ligne
						if(json.service=="A"){
							$("#serviceA"+j).html("<img src='image/BT.png' alt='sevice' height='25px' width='25px'/>");
							$("#serviceB"+j).html("");
						}
						else{
							$("#serviceB"+j).html("<img src='image/BT.png' alt='sevice' height='25px' width='25px'/>");
							$("#serviceA"+j).html("");
						}
						
						$.each( json.set, function( key, val )
						{
								i++;
								$("#setA"+j+i).html("<b>"+val.A+"</b>");
								
						});
						
						while(i>nbsetA[j-1]){
							$("#setA"+j+nbsetA[j-1]).after("<td id='setA"+j+(nbsetA[j-1]+1)+"' class='ss texte'><b>"+json.set[nbsetA[j-1]].A+"</b></td>");
							nbsetA[j-1]++;
						}
						
						while(i<nbsetA[j-1]){
							$("#setA"+j+nbsetA[j-1]).remove();
							nbsetA[j-1]--;
						}
						
			
						i=0;
						
						
						if(json.scoreA=="4444"){
							$("#scoreA"+j).html("<b>ADV</b>");
						}
						else
						{
						 if(json.vainqueur=="A"||json.vainqueur=="B")
							{
							  $("#scoreA"+j).replaceWith("<td  id= 'scoreA"+j+"' class='ss'></td>");
							}
							else
							{
							 $("#scoreA"+j).html("<b>"+json.scoreA+"</b>");
							}
						}
						
						
						//ligne2
						
						$.each( json.set, function( key, val )
						{
								i++;
								$("#setB"+j+i).html("<b>"+val.B+"</b>");
								
						});
						
						while(i>nbsetB[j-1]){
							$("#setB"+j+nbsetB[j-1]).after("<td id='setB"+j+(nbsetB[j-1]+1)+"' class='ss texte'><b>"+json.set[nbsetB[j-1]].B+"</b></td>");
							nbsetB[j-1]++;
						}
						while(i<nbsetB[j-1]){
							$("#setB"+j+nbsetB[j-1]).remove();
							nbsetB[j-1]--;
						}
						i=0;
						if(json.scoreB=="4444"){
							$("#scoreB"+j).html("<b>ADV</b>");
						}
						else{
							if(json.vainqueur=="A"||json.vainqueur=="B")
							{
							  $("#scoreB"+j).replaceWith("<td  id= 'scoreB"+j+"' class='ss'></td>");
							}
							else
							{
							 $("#scoreB"+j).html("<b>"+json.scoreB+"</b>");
							 }
						}
					
					}	
					  
				});//fin remplissage des Tableaux 
			});
		}
		//appelle de la fonction terrain toutes les secondes
		
		terrain();
		setInterval(terrain, 2000);	
		
});