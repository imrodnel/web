
jQuery(document).ready(function()
{      		var tab = [1, 2, 3, 4, 5, 6, 7];
			var nbsetB = [1,1,1,1,1,1,1,1];
			var url = "https://5.196.21.161:2223/courts/infos/" ;
			//var adr ="fichier.json";
			var adr;
			var i=0;
		    var nomJoueurB="";
			var nomJoueurA="";
			var nomJoueurB="";
			var nomJFinalA="";
			var nomJFinalB="";
		   
		 //remplissage des tableaux et mise a jours toutes les 2 secondes
		 function terrain() 
		 {
			$.each(tab, function (indice, j)
			{
				adr=url+j;
				$.getJSON(adr, function(json)
					{ 	
						var code= json.code;
						if(code!=0){
						   $("#time"+j).html("");
						   $("#cat"+j).html("");
						   $("#natA"+j).html("");
						   $("#natB"+j).html("");
						   $("#natA"+j).html("");
						   $("#serviceA"+j).html("");
						   $("#serviceB"+j).html("");
						   $("#nomA"+j).html("");
						   $("#nomB"+j).html("");
						   $("#scoreA"+j).html("");
						   $("#scoreB"+j).html("");
						   $("#setA"+j+1).html("");
						   $("#setB"+j+1).html("");
						   $("#setA"+j+2).html("");
						   $("#setB"+j+2).html("");
						   $("#setA"+j+3).html("");
						   $("#setB"+j+3).html("");
						   $("#setA"+j+4).html("");
						   $("#setB"+j+4).html("");
						   $("#setA"+j+5).html("");
						   $("#setB"+j+5).html("");
						   $("#photoA"+j).html("<img src='image/person.png' width='80' height='114'>");
						   $("#photoB"+j).html("<img src='image/person.png' width='80' height='114'>");
						}
						else{
							var court= json.court;
							//chargement des photos des joueurs
							if(court.Category=="DM"||court.Category=="DW"||court.Category=="DM"){
								var time = court.Duration;
								var step = "3";
								var minutes = time.split(":")[1];
								var k = Math.floor(minutes / step);
								
								if(k % 2 == 0){
									var photoA= court.photoA1;
									if(photoA!=null){
										$("#photoA"+j).html("<img src='"+photoA+"' width='80' height='114'>");
									}
									var photoB= court.photoB1;
									if(photoB!=null){
										$("#photoB"+j).html("<img src='"+photoB+"' width='80' height='114'>");
									}
								}
								else{
									var photoA= court.photoA2;
									if(photoA!=null){
										$("#photoA"+j).html("<img src='"+photoA+"' width='80' height='114'>");
									}
									var photoB= court.photoB2;
									if(photoB!=null){
										$("#photoB"+j).html("<img src='"+photoB+"' width='80' height='114'>");
									}
								}
							}
							else{
								var photoA= court.photoA;
								if(photoA!=null){
									$("#photoA"+j).html("<img src='"+photoA+"' width='80' height='114'>");
								}
								var photoB= court.photoB;
								if(photoB!=null){
									$("#photoB"+j).html("<img src='"+photoB+"' width='80' height='114'>");
								}
							}//fin chargement des photos
							
							//chargement des noms des joueurs
							if(court.NameA.length > 18)
									{
											nomJoueurA = court.NameA.split("/");
										 for(var e = 0 ; e < nomJoueurA.length; e++)
										 {
											if(nomJoueurA[e].length > 9)
											{
												nomJFinalA =nomJFinalA+ nomJoueurA[e].substring(0,7) + ".";
											}
											else
												nomJFinalA =  nomJFinalA+nomJoueurA[e];
											if(e==0)
												nomJFinalA = nomJFinalA + "/";
										  }
										  
									}
									else
									{
										nomJFinalA = court.NameA;
									}
									if(court.NameB.length >= 18)
									{
										  nomJoueurB = court.NameB.split("/");
										 for(var e = 0 ; e < nomJoueurB.length; e++)
										 {
											if(nomJoueurB[e].length > 9)
												nomJFinalB = nomJFinalB + nomJoueurB[e].substring(0,7) + ".";
											else
												nomJFinalB = nomJFinalB +nomJoueurB[e];
											if(e==0)
												nomJFinalB = nomJFinalB + "/";
										 }
										 
									}
									else
									{
										nomJFinalB = court.NameB;
									}
							$("#nomA"+j).html(nomJFinalA);
							$("#nomB"+j).html(nomJFinalB);
							nomJFinalB ="";
							nomJFinalA=""; 
							if(court.Winner=="A"){
								$("#nomA"+j).css('color','green');
								$("#nomB"+j).css('color','red');
							}
							if(court.Winner=="B"){
								$("#nomA"+j).css('color','red');
								$("#nomB"+j).css('color','green');
							}//fin chargement des noms
							
							//chargement du type de match
							switch(court.Category) {
										case "SM":
											$("#cat"+j).html("<i>Men's Single</i>");
											break;
										case "SW":
											$("#cat"+j).html("<i>Women's Single</i>");
											break;
										case "DM":
											$("#cat"+j).html("<i>Men's Double</i>");
											break;
										case "DW":
											$("#cat"+j).html("<i>Women's Double</i>");
											break;
										case "DM":
											$("#cat"+j).html("<i>Mixted Double</i>");
											break;
										default: $("#cat"+j).html("");
							}// fin 
							
							//duree du match
							$("#time"+j).html("<i>"+court.Duration+"</i>");
							// niveau dans le tournois
							$("#tableau").html(court.Tableau);
							//nationnalité des joueurs
							$("#natA"+j).html("<img src='image/"+court.TeamA+".jpg' alt='"+court.TeamA+"' width='30px'/>");
							$("#natB"+j).html("<img src='image/"+court.TeamB+".jpg' alt='"+court.TeamB+"' width='30px'/>");
							
							// qui a le service 
							if(court.Service=="A"){
									$("#serviceA"+j).html("<img src='image/BT.png' alt='sevice'  width='25px'/>");
									$("#serviceB"+j).html("");
								}
							else{
									$("#serviceB"+j).html("<img src='image/BT.png' alt='sevice'  width='25px'/>");
									$("#serviceA"+j).html("");
								}//fin
								
							//chargement des scores
							if(court.Winner==null){
								if(court.ScoreA== 4444){
										$("#scoreA"+j).html("<b>[ADV]</b>");
									}
								else
									{
										 $("#scoreA"+j).html("<b> ["+court.ScoreA+"]</b>");
									}
								if(court.ScoreB== 4444){
										$("#scoreB"+j).html("<b> [ADV]</b>");
									}
								else{
										 $("#scoreB"+j).html("<b> ["+court.ScoreB+"]</b>"); 
									}
								}
							else{
								$("#scoreA"+j).html("<b>[ ]</b>");
								$("#scoreB"+j).html("<b>[ ]</b>");
							}//fin chargement des scores
							
							// chargement des sets 
							$.each( court.Set, function( key, val )
							{
								i++;
								$("#setA"+j+i).html("<b>"+val.A+"</b>");
								$("#setB"+j+i).html("<b>"+val.B+"</b>");
								
							});
						
							while(i>nbsetB[j]){
								$("#setA"+j+nbsetB[j]).after("<td id='setA"+j+(nbsetB[j]+1)+"' ><b>"+court.Set[nbsetB[j]].A+"</b></td>");
								$("#setB"+j+nbsetB[j]).after("<td id='setB"+j+(nbsetB[j]+1)+"' ><b>"+court.Set[nbsetB[j]].B+"</b></td>");
								nbsetB[j]++;
							}
							while(i<nbsetB[j]){
								$("#setA"+j+nbsetB[j]).remove();
								$("#setB"+j+nbsetB[j]).remove();
								nbsetB[j]--;
							}	
							i=0;
							// fin chargement des sets 
						}
					});
				});
		}
		//appelle de la fonction terrain toutes les secondes
		
		terrain();
		setInterval(terrain, 2000);	
		
});