jQuery(document).ready(function()
{
	
	var adr = "http://5.196.21.161:8080/middlewareTennis/match/super/4" ;
	// var adr = "http://37.187.238.24:8080/middlewareTennis/match/super/2" ;
	var tab = new Array() ; // permet de connaitre ou se situe la rencontre parmis les colonnes
	// permet de savoir pour chacune des rencontres a combien de set on en est (est utiliser pour determiner si il faut ajouter un set en plus ou non)
	var tabrencontre1_Set = new Array() ; 
	var tabrencontre2_Set = new Array() ;
	var tabrencontre3_Set = new Array() ;
	var tabrencontre4_Set = new Array() ;
	
	var numRen;
	var nbRen;
	
	// on initialise les tableaux
	function initTab() 
	{ 
		for (i = 0; i < 7; i++) 
		{ 
			tabrencontre1_Set[i] = new Array();
			tabrencontre2_Set[i] = new Array();
			tabrencontre3_Set[i] = new Array();
			tabrencontre4_Set[i] = new Array();
		}
		
		for (i = 0; i < 7; i++) 
		{ 
			for (j = 0; j < 3; j++) 
			{ 
				tabrencontre1_Set[i][j] = 0;
				tabrencontre2_Set[i][j] = 0;
				tabrencontre3_Set[i][j] = 0;
				tabrencontre4_Set[i][j] = 0;
				
			}
		}
	}
		
	function construction() // contrui
	{ 
		initTab();
		nbRen = 0 ;
		
		// on récupere le fichier json
		$.getJSON(adr, function(json)
		{
			// on parcours les differentes rencontres
			$.each(json.rencontre, function(key, renc)
			{
				numRen = renc.IdSupmatch; // on recupere le num de la rencontre
				tab[nbRen] = numRen ; // on l'ajoute dans le tab pour cela nous permet de savoir ou il sera situer dans le colonnes
				
				//on commence a creer notre rencontre 
				var chaine = "<div class='uneRencontre' ><div class='titleCountry'><h1 id='R"+numRen+"titrePays'></h1></div>";
				var numSet = 1; // contiendra le nombre de set;
				var numMatch = 0; 
				// on parcour les matchs de la rencontre
				$.each(renc.matchs, function(key, val)
				{
					// on cree la structure pour les differents matchs
					chaine = chaine + "<div class='div_match'><span class='titleMatch' id='R"+numRen+"match"+val.IdMatch+"'></span>";
					chaine = chaine + "<table class='tabRencontre' id='tableR"+numRen+"match"+val.IdMatch+"'>";
					
					
					// on teste si le match est en cours ou terminer car si il est avenir on doit afficher SOON
					if(val.Statut == "EN_COURS" || val.Statut == "TERMINE")
					{
						//on creer la structure pour les different  set
						//pour le joueur A
						chaine = chaine + "<tr><td class='tdnomJ' id='R"+numRen+"drapeauxA"+val.IdMatch+"'></td>";
						numSet = 1;
						$.each(val.set, function(set, score_set)
						{	if(score_set.A != -1) // on test si le set a deja commencer
							{
								chaine= chaine + "<td class='tdscoreset' id='R"+numRen+"match"+val.IdMatch+"setA"+numSet+"'></td>";
								numSet++;
							}
						});
						chaine= chaine + "<td  class='tdscore' id='R"+numRen+"scoreA"+val.IdMatch+"'></td>";
						//pour le joueur B
						chaine = chaine + "</tr><tr><td class='tdnomJ' id='R"+numRen+"drapeauxB"+val.IdMatch+"'></td>";
						numSet = 1;
						$.each(val.set, function(set, score_set)
						{
							if(score_set.A != -1)
							{
								chaine= chaine + "<td class='tdscoreset' id='R"+numRen+"match"+val.IdMatch+"setB"+numSet+"'></td>";
								numSet++;
							}
						});
						chaine= chaine + "<td class='tdscore' id='R"+numRen+"scoreB"+val.IdMatch+"'></td></tr>";
					
					}
					else if(val.Statut == "A_VENIR")
					{
						chaine = chaine + "<tr><td class='soon' id='R"+numRen+"soon"+val.IdMatch+"'>SOON</td></tr>";
					}
					// on ferme le te tableau et la div du match qu'on vien de creer
					chaine = chaine + "</table></div>";
					
					// la on va enrengistre les set pour les differents matchs
					if( nbRen == 0 )
					{
						// on enregistre le num du match, le nombre de set pour ce match, et le status du match
						tabrencontre1_Set[numMatch][0] = val.IdMatch; 
						tabrencontre1_Set[numMatch][1] = numSet - 1;
						tabrencontre1_Set[numMatch][2] = val.Statut;
					}
					else if( nbRen == 1 )
					{
						
						tabrencontre2_Set[numMatch][0] = val.IdMatch;
						tabrencontre2_Set[numMatch][1] = numSet - 1;
						tabrencontre2_Set[numMatch][2] = val.Statut;
					}
					else if( nbRen == 2 )
					{
					
						tabrencontre3_Set[numMatch][0] = val.IdMatch;
						tabrencontre3_Set[numMatch][1] = numSet - 1;
						tabrencontre3_Set[numMatch][2] = val.Statut;
					}
					else if( nbRen == 3 )
					{
					
						tabrencontre4_Set[numMatch][0] = val.IdMatch;
						tabrencontre4_Set[numMatch][1] = numSet - 1;
						tabrencontre4_Set[numMatch][2] = val.Statut;
					}
					
					chaine = chaine + "<hr class='ligneWhite'/>"; // on met une ligne de separation entre les match
					numMatch++;
				});
			
				
				
				chaine = chaine + "</div>";
				nbRen++;
				$("#rencontre"+ nbRen).append(chaine); // on ajouter la structure a la colonne rencontre
				
				
			});

		});
	}
	function rencontre() // fonction qui permet de modifier le contenue des matchs
	{ 
		
		$.getJSON(adr, function(json)
		{
			//$("#position").html(json.position);
		
			// on parcourt les rencontre
			$.each(json.rencontre, function(key, renc)
			{
				// on ajoute le nom des pays A ainsi que leur drapeaux , leur score et leur affactation dans le tableau
				$("#R"+renc.IdSupmatch+"titrePays").html("<img src='image/"+renc.Pays_A+".jpg'> " + pays(renc.Pays_A )+" "+renc.ScorePays_A+" - "+renc.ScorePays_B+" "+ pays(renc.Pays_B) + " <img src='image/"+renc.Pays_B+".jpg'> </br> <span class='soustitre'>" + afftable(renc.Tableau ) + "</span>");
				var nom = ""; // contient le nom du match 
				var numSet = 1;
				
				$.each(renc.matchs, function(key, val)
				{
					// on verifie le status du match, pour savoir si il a etais modifier ex: passer de A_venir a encours
					if(nowStatut(NowRencontre(renc.IdSupmatch), val.IdMatch) == val.Statut )
					{
						// on modifier le nom du match ex SH = simple homme
						if(val.Categorie == "SH")
								nom = nomMatch(val.Categorie) + " N° " + (val.IdMatch%7) ;
						else if(val.Categorie == "SF")
							nom = nomMatch(val.Categorie) + " N° " + ((val.IdMatch-2)%7) ;
						else 
							nom = nomMatch(val.Categorie);
						
						
						if(val.Statut == "EN_COURS") // si le status est en cour il faut le preciser a l affichage
						{
							$("#R"+renc.IdSupmatch+"match"+val.IdMatch).html(nom + " - Court " + val.Court);
						}
						else
						{
							$("#R"+renc.IdSupmatch+"match"+val.IdMatch).html(nom);
						}
						
						if(val.Statut == "EN_COURS" || val.Statut == "TERMINE")
						{
							// on va teste la longueur des noms des joueurs
							var nomJA = val.Joueurs_A;
							var nomJB = val.Joueurs_B;
							var nomJFinalA = ""; // sera le nom final qu on affichera a l ecran
							var nomJFinalB = "";
							
							if(nomJB.length > 20)
							{
								 nomJB = nomJB.split("/");
								 for(var e = 0 ; e < nomJB.length; e++)
								 {
									if(nomJB[e].length > 10)
									{
										nomJFinalB = nomJFinalB + nomJB[e].substring(0,7) + ".";
									}
									else
										nomJFinalB = nomJFinalB + nomJB[e];
									if(e==0)
										nomJFinalB = nomJFinalB + "/";
								 }
								 
								
							}
							else
							{
								nomJFinalB = nomJB;
							}
							if(nomJA.length >= 20)
							{
									nomJA = nomJA.split("/");
								 for(var e = 0 ; e < nomJA.length; e++)
								 {
									if(nomJA[e].length > 10)
										nomJFinalA = nomJFinalA + nomJA[e].substring(0,7) + ".";
									else
										nomJFinalA = nomJFinalA + nomJA[e];
									if(e==0)
										nomJFinalA = nomJFinalA + "/";
								 }
							}
							else
							{
								nomJFinalA = nomJA;
							}
							
						
							// si on connais le vainqueur alors on le colore en vert
							if(val.Vainqueur == "A")
								$("#R"+renc.IdSupmatch+"drapeauxA"+val.IdMatch).html("<img src='image/"+renc.Pays_A+".jpg'> <span class='nomJGagne'>"+ nomJFinalA + " </span>");
							else
								$("#R"+renc.IdSupmatch+"drapeauxA"+val.IdMatch).html("<img src='image/"+renc.Pays_A+".jpg'> <span class='nomJ'>"+ nomJFinalA + " </span>");
								
							if(val.Vainqueur == "B")	
								$("#R"+renc.IdSupmatch+"drapeauxB"+val.IdMatch).html("<img src='image/"+renc.Pays_B+".jpg'> <span class='nomJGagne'>"+ nomJFinalB + " </span>");
							else
								$("#R"+renc.IdSupmatch+"drapeauxB"+val.IdMatch).html("<img src='image/"+renc.Pays_B+".jpg'> <span class='nomJ'>"+ nomJFinalB + " </span>");
							

							// on ajoute le score actuelle du match 15 30 etc..
							
							$("#R"+renc.IdSupmatch+"scoreA"+val.IdMatch).html(score(val.Score_A));
							$("#R"+renc.IdSupmatch+"scoreB"+val.IdMatch).html(score(val.Score_B));
							
							numSet = 1;
							
							// on va parcourir les set pour les modifiers
							$.each(val.set, function(set, score_set)
							{
								if(score_set.A != -1) // on teste si le set est jouer
								{
										
									if(nbSetNow(NowRencontre(renc.IdSupmatch), val.IdMatch) < numSet) // on verifie si il y a plus de set qu avant car il faut ajouter une colonne
									{
										var n = (numSet - 1) ;
										$("#R"+renc.IdSupmatch+"match"+val.IdMatch+"setA"+n).after("<td class='tdscoreset' id='R"+renc.IdSupmatch+"match"+val.IdMatch+"setA"+numSet+"'>"+score_set.A+"</td>");
										$("#R"+renc.IdSupmatch+"match"+val.IdMatch+"setB"+n).after("<td class='tdscoreset' id='R"+renc.IdSupmatch+"match"+val.IdMatch+"setB"+numSet+"'>"+score_set.B+"</td>");
										modifiSet(NowRencontre(renc.IdSupmatch), val.IdMatch, numSet);
									}
									else
									{
										
										$("#R"+renc.IdSupmatch+"match"+val.IdMatch+"setA"+numSet).html(score_set.A);
										$("#R"+renc.IdSupmatch+"match"+val.IdMatch+"setB"+numSet).html(score_set.B);
									}
									numSet++;
								}
							});
						}
					}
					else
					{
						// permet de recontruitre un match car il a changer 
						reconstruction(renc.IdSupmatch,val.IdMatch);
						
					}
					
				});
				
			  
			});
		});
	}
	
	
	function reconstruction(ren, mat) 
	{ 
		
		
		$.getJSON(adr, function(json)
		{
			
			$.each(json.rencontre, function(key, renc)
			{
				if(ren == renc.IdSupmatch)
				{
					numRen = renc.IdSupmatch;
					
					var chaine = "";
					var numSet = 1;
					var numMatch = 0;
					$.each(renc.matchs, function(key, val)
					{
						if(mat == val.IdMatch)
						{
							if(val.Statut == "EN_COURS" || val.Statut == "TERMINE")
							{
								chaine = chaine + "<tr><td class='tdnomJ' id='R"+numRen+"drapeauxA"+val.IdMatch+"'></td>";
								numSet = 1;
								$.each(val.set, function(set, score_set)
								{	
									if(score_set.A != -1)
									{
										chaine= chaine + "<td class='tdscoreset' id='R"+numRen+"match"+val.IdMatch+"setA"+numSet+"'></td>";
										numSet++;
									}
								});
								chaine= chaine + "<td  class='tdscore' id='R"+numRen+"scoreA"+val.IdMatch+"'></td>";
								chaine = chaine + "</tr><tr><td class='tdnomJ' id='R"+numRen+"drapeauxB"+val.IdMatch+"'></td>";
								numSet = 1;
								$.each(val.set, function(set, score_set)
								{
									if(score_set.A != -1)
									{
										chaine= chaine + "<td class='tdscoreset' id='R"+numRen+"match"+val.IdMatch+"setB"+numSet+"'></td>";
										numSet++;
									}
								});
								
								chaine= chaine + "<td class='tdscore' id='R"+numRen+"scoreB"+val.IdMatch+"'></td></tr>";
							
							}
							else if(val.Statut == "A_VENIR")
							{
								chaine = chaine + "<tr><td class='soon' id='R"+numRen+"soon"+val.IdMatch+"'>SOON</td></tr>";
							}

							nbRen = NowRencontre( renc.IdSupmatch);
							var result = 0 ;
							var trouver = 0 ;
							var i = 0;
							if( nbRen == 0 )
							{	
								while( i<7 && trouver != 1 )
								{
									if(tabrencontre1_Set[i][0] == val.IdMatch)
									{
										tabrencontre1_Set[numMatch][1] = numSet - 1;
										tabrencontre1_Set[numMatch][2] = val.Statut;
										trouver = 1;
									}
									else
										i++;
								}
							}
							else if( nbRen == 1 )
							{
								while( i<7 && trouver != 1 )
								{
									if(tabrencontre2_Set[i][0] == val.IdMatch)
									{
										tabrencontre2_Set[numMatch][1] = numSet - 1;
										tabrencontre2_Set[numMatch][2] = val.Statut;
										trouver = 1;
									}
									else
										i++;
								}
							}
							else if( nbRen == 2 )
							{
							
								while( i<7 && trouver != 1 )
								{
									if(tabrencontre3_Set[i][0] == val.IdMatch)
									{
										tabrencontre3_Set[numMatch][1] = numSet - 1;
										tabrencontre3_Set[numMatch][2] = val.Statut;
										trouver = 1;
									}
									else
										i++;
								}
							}
							else if( nbRen == 3 )
							{
							
								while( i<7 && trouver != 1 )
								{
									if(tabrencontre4_Set[i][0] == val.IdMatch)
									{
										tabrencontre4_Set[numMatch][1] = numSet - 1;
										tabrencontre4_Set[numMatch][2] = val.Statut;
										trouver = 1;
									}
									else
										i++;
								}
							}
							
							numMatch++;
						}
					});
				
					
					
					
					nbRen++;
					$("#tableR"+ren+"match"+mat).html(chaine);
				
				}
			});

		});
	}
	
	
	function init() 
	{
		construction();
		rencontre();
		setInterval(rencontre,5000);
		// setInterval(changePage,15000);
		
	}
	
	init();
	
	
	
	function score(nb) 
	{
		if(nb == 4444)
		{
			nb = 'ADV';
		}
		return nb ;
	}
	
	
	function nomMatch(Categorie) 
	{
		switch (Categorie) 
		{
			case "SH":
				Categorie = "Men's Single";
			break;
			case "SF":
				Categorie = "Women's Single";
			break;
			case "DM":
				Categorie = "Mixted Double";
			break;
			case "DF":
				Categorie = "Women's Double";
			break;
			case "DH":
				Categorie = "Men's Double";
			break;
			
		}
		
		return Categorie ;
	}
	
	
	function afftable(t) 
	{
		switch (t) 
		{
			case "1/4-P":
				t = "Quarterfinals";
			break;
			case "'1/2-P":
				t = "Semifinals Main";
			break;
			case "1/2-5":
				t = "Semifinals - 5th place";
			break;
			case "Finale-P":
				t = "Finals";
			break;
			case "'Finale-3":
				t = "Finals - 3rd place";
			break;
			case "'Finale-5":
				t = "Finals - 5th place";
			break;
			case "'Finale-7":
				t = "Finals - 7th place";
			break;
			
		}
		
		return t ;
	}
	
	
	 //la fonction prend en paramettre nation. on l'appelle avec 'json.nat' 
	 function pays(nation)
	{   
		var c = "";  
		switch(nation) 
		{
			case "FRA":
				c = "France";
				break;
			case "USA":
				c = "USA";
				break;
			case "GBR":
				c = "Great Britain";
				break;
			case "CHN":
				c = "China";
				break;
			case "DEU":
				c = "Germany";
				break;
			case "BEL":
				c = "Belgium";
				break;
			case "IRL":
				c = "Ireland";
				break;
			case "RUS":
				c = "Russia";
				break;
			default: c = "";
		}
		
		return c ;
	}
	
	//function qui permet de nous dire ou se trouve la rencontre actuelle
	function NowRencontre(Ren)
	{
		var result = 0 ;
		var trouver = 0 ;
		var i = 0;
		
		while( i<4 && trouver != 1 )
		{	
			if(tab[i] == Ren)
			{
					result = i;
					trouver = 1 ;
			}
			else
				i++;
		}
		
		return i ;
	
	}
	
	// permet de nous a combien on en a de set actuellement
	function nbSetNow(Ren, mat)
	{  
	
		var result = 0 ;
		var trouver = 0 ;
		var i = 0;
		
		if( Ren == 0 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre1_Set[i][0] == mat)
				{
					result = tabrencontre1_Set[i][1];
					trouver = 1 ;
				}
					
				i++;
			}
		}
		else if( Ren == 1 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre2_Set[i][0] == mat)
				{
					result = tabrencontre2_Set[i][1];
					trouver = 1 ;
				}
				
				i++;
			}
		}
		else if( Ren == 2 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre3_Set[i][0] == mat)
				{
					result = tabrencontre3_Set[i][1];
					trouver = 1 ;
				}
				
				i++;
			}
		}
		else if( Ren == 3 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre4_Set[i][0] == mat)
				{
					result = tabrencontre4_Set[i][1];
					trouver = 1 ;
				}
				
				i++;
			}
		}
		
		return result;
	}
	
	
	function nowStatut(Ren, mat)
	{  
	
		var result = "now";
		var trouver = 0 ;
		var i = 0;
		
		if( Ren == 0 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre1_Set[i][0] == mat)
				{
					result = tabrencontre1_Set[i][2];
					trouver = 1 ;
				}
					
				i++;
			}
		}
		else if( Ren == 1 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre2_Set[i][0] == mat)
				{
					result = tabrencontre2_Set[i][2];
					trouver = 1 ;
				}
				
				i++;
			}
		}
		else if( Ren == 2 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre3_Set[i][0] == mat)
				{
					result = tabrencontre3_Set[i][2];
					trouver = 1 ;
				}
				
				i++;
			}
		}
		else if( Ren == 3 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre4_Set[i][0] == mat)
				{
					result = tabrencontre4_Set[i][2];
					trouver = 1 ;
				}
				
				i++;
			}
		}
		
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	function aff(Ren, mat, set)
	{
		for (i = 0; i < 7; i++) 
		{ 
			for (j = 0; j < 2; j++) 
			{ 
				$("#erreur").append("<br/> tabrencontre1_Set[" + i + "][" + j + "] : " + tabrencontre1_Set[i][j] );
			}
		}
	
	}
	function modifiSet(Ren, mat, set)
	{  
		var trouver = 0 ;
		var i = 0;
		if( Ren == 0 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre1_Set[i][0] == mat)
				{
					tabrencontre1_Set[i][1] = set;
					trouver = 1 ;
				}
					
				i++;
			}
		}
		else if( Ren == 1 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre2_Set[i][0] == mat)
				{
					tabrencontre2_Set[i][1] = set;;
					trouver = 1 ;
				}
				
				i++;
			}
		}
		else if( Ren == 2 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre3_Set[i][0] == mat)
				{
					tabrencontre3_Set[i][1] = set;
					trouver = 1 ;
				}
				
				i++;
			}
		}
		else if( Ren == 3 )
		{
			while( i<7 && trouver != 1 )
			{
				if(tabrencontre4_Set[i][0] == mat)
				{
					tabrencontre4_Set[i][1] = set;
					trouver = 1 ;
				}
				
				i++;
			}
		}
		
	}
});