jQuery(document).ready(function()
{
	var adr="http://5.196.21.161:8080/middlewareTennis/match/infosTableau";
	//var adr="fichier.json";
    function pays(s)
	{ 
	    var p;
		switch(s) {
			case "FRA": p="France";
				break;
			case "DEU": p="Germany";
				break;
			case "USA": p="Usa";
				break;
			case "BEL": p="Belgium";
				break;
			case "IRL": p="Ireland";
				break;
			case "CHN": p="China";
				break;
			case "RUS": p="Russia";
				break;
			case "GBR": p="Britain";
				break;
			default: p="";
		}
		return p;
	}
	
	function tableau()
	{
		$.getJSON(adr, function(json)
		{
		    if(json.code==0){
				$.each(json.bracket, function( key, val )
				{ 
				   var payA=pays(val.Team_A);
				   var payB=pays(val.Team_B);
					if((val.Score_A+val.Score_B)==7){
						if(val.Score_A>val.Score_B ){
						$("#m"+val.IdHugeMatch).html("<div class='pays'><div class='gauche'><img class='drapeaux' src='image/"+val.Team_A+".jpg' align='left'><span class='textegagnant' id='paysA_finalLoser'>"+payA+"</span>	</div><div class='droite'><img class='drapeaux' src='image/"+val.Team_B+".jpg' align='left'><span class='texteperdant' id='paysB_finalLoser'>"+payB+"</span> </div></div><p align='center' ><span class='scoregagnant' >"+val.Score_A+"</span>  - <span class='scoreperdant'> "+val.Score_B+"</span> </p>");
						}
						else{
						$("#m"+val.IdHugeMatch).html("<div class='pays'><div class='gauche'><img class='drapeaux' src='image/"+val.Team_A+".jpg' align='left'><span class='texteperdant' id='paysA_finalLoser'>"+payA+"</span>	</div><div class='droite'><img class='drapeaux' src='image/"+val.Team_B+".jpg' align='left'><span class='textegagnant' id='paysB_finalLoser'>"+payB+"</span> </div></div><p align='center' ><span class='scoreperdant' >"+val.Score_A+"</span>  - <span class='scoregagnant'> "+val.Score_B+"</span> </p>");
						}
					}
				  else{
						$("#m"+val.IdHugeMatch).html("<div class='pays'><div class='gauche'><img class='drapeaux' src='image/"+val.Team_A+".jpg' align='left'><span class='texte' id='paysA_finalLoser'>"+payA+"</span>	</div><div class='droite'><img class='drapeaux' src='image/"+val.Team_B+".jpg' align='left'><span class='texte' id='paysB_finalLoser'>"+payB+"</span> </div></div><p align='center' ><span class='score' >"+val.Score_A+"</span>  - <span class='score'> "+val.Score_B+"</span> </p>");
					} 
				});
			}
			else{
				$.each(json.bracket, function( key, val )
				{ 
					$("#m"+val.IdHugeMatch).html("");
				});
			}
		});
	}
	
	tableau();
	setInterval(tableau, 6000);	

});